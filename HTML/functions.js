var cpuGauge = null;
var tempGauge = null;
var usageGauge = null;
var costGauge = null;

var addResult = function(data, series) {

	for (var index in data) {
		var object = data[index];
		
		if (object.value == null)
			continue;

		var found = false;

		object.timestamp = object.timestamp * 1000;
		
		var xDataLength = chart.series[series].xData.length;
		
		var lastValue = 0;
		if (xDataLength > 0)
			lastValue = chart.series[series].xData[xDataLength - 1];
			
		if (object.timestamp > lastValue && object.value) {
			var shouldShift = (xDataLength > 60);
			chart.series[series].addPoint([object.timestamp, object.value], false, shouldShift);
		}
	}
	
	chart.redraw();
};

var getPoint = function(series, startDate, endDate, resolution) {
	var startDateString = (startDate.getTime() / 1000).toFixed(0).toString();
	
	var endDateString = ""; //
	if (endDate != "0") {
		endDateString = (endDate.getTime() / 1000).toFixed(0).toString();
	}
	else {
		endDateString = "0"						
	}
	
	$.getJSON("/api/node:read/0/temp/" + resolution + "/" + startDateString + "/" + endDateString +"?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
        dataObject = JSON.parse(data);
		addResult(dataObject, 0);
	});	

	$.getJSON("/api/node:read/0/power.current.usage/" + resolution + "/" + startDateString + "/" + endDateString + "?apikey=abcdefghijklmop", {},
	function(data) {
        dataObject = JSON.parse(data);
		addResult(dataObject, 1);
	});
};

var getCPU = function() {
	$.getJSON("/api/node:last/255/cpuload/1?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
        dataObject = JSON.parse(data);
		cpuGauge.refresh(dataObject.value.toFixed());
	});	
};

var getLastTemp = function() {
	$.getJSON("/api/node:last/0/temp/1?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
        dataObject = JSON.parse(data);
		tempGauge.refresh(dataObject.value.toFixed(2));
	});						
}

var getLastUsage = function() {
	$.getJSON("/api/node:last/0/power.current.usage/1?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
        dataObject = JSON.parse(data);
		usageGauge.refresh(dataObject.value.toFixed());
	});						
}

var getLastMemoryUsage = function() {
   	$.getJSON("/api/node:last/255/memory.virtual/1?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
        dataObject = JSON.parse(data);
		costGauge.refresh(dataObject.value.toFixed());
	});
}

$(document).ready(function() {

	cpuGauge = new JustGage({
		id: "cpu-gauge", 
		value: 0, 
		min: 0,
		max: 100,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		shadowSize: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "CPU load"
	}); 
	
	tempGauge = new JustGage({
		id: "temp-gauge", 
		value: 0, 
		min: 0,
		max: 30,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "",
        label: "celcius"
	}); 
	
	usageGauge = new JustGage({
		id: "usage-gauge", 
		value: 0, 
		min: 0,
		max: 8000,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "Power usage",
        label: "Watts"
	}); 
	
	costGauge = new JustGage({
		id: "cost-gauge",
		value: 0, 
		min: 0,
		max: 100,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "Memory Usage %"
	}); 	
	
  	getCPU();
    setInterval(function() {
        getCPU();
    }, 5000);

    getLastTemp();
    setInterval(function() {
    	getLastTemp();
    }, 5000);
    
    getLastUsage();
    setInterval(function() {
    	getLastUsage();
    }, 5000);

    getLastMemoryUsage();
    setInterval(function() {
        getLastMemoryUsage();
    }, 5000)
});