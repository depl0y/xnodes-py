import os
from Managers.Log import Log
import configparser

class ConfigManager(object):
	
	ApplicationPath = os.getcwd()

	def __init__(self):
		pass
		
	def getStorageDirectory(self):
		path = ConfigManager.ApplicationPath
		path = os.path.join(path, "Storage")
		self.createDirectory(path)
		return path
	
	def getSqlDatabasePath(self):
		path = self.getStorageDirectory();
		path = os.path.join(path, "xnodes.db")
		return path
	
	def getSqlCreateScript(self):
		path = ConfigManager.ApplicationPath
		path = os.path.join(path, "Init", "create-db.sql")
		return path
		
	def createDirectory(self, directoryName):
		if not os.path.isdir(directoryName):
			Log.Instance().verbose(__name__, "Creating directory " + directoryName)
			os.makedirs(directoryName)
			
	def rrdPath(self, nodeId, dataType):
		storagePath = self.getStorageDirectory()
		rrd = os.path.join(storagePath, "nodes", str(nodeId))
		self.createDirectory(rrd)
		rrd = os.path.join(rrd, dataType + ".rrd")
		return rrd
			
	def baseUrl(self):
		config = configparser.ConfigParser()
		config.read("config")
		host = "localhost:" + config["API"]["Port"]
		return host
	
	def httpPort(self):
		config = configparser.ConfigParser()
		config.read("config")
		return config["API"]["Port"]


		