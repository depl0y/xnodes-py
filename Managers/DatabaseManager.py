import sqlite3
import os
from Classes.Singleton import Singleton
from Managers.ConfigManager import ConfigManager
from Managers.Log import Log

@Singleton
class DatabaseManager:
    
    def __init__(self):
        Log.Instance().info(__name__, "Initializing DatabaseManager")
        self.databasePath = ConfigManager().getSqlDatabasePath()
        
        if not os.path.exists(self.databasePath):
        	Log.Instance().verbose(__name__, "Database does not exist, creating...")
        	sqlScript = open(ConfigManager().getSqlCreateScript()).read()
        	self.connection = sqlite3.connect(self.databasePath)
        	self.connection.executescript(sqlScript)
        else:
        	self.connection = sqlite3.connect(self.databasePath)
            
    def close(self):
    	Log.Instance().info(__name__, "Closing database")
    	self.connection.close()
        