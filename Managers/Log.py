from Classes.bcolors import bcolors
from Classes.Singleton import  Singleton
import datetime
import logging

@Singleton
class Log:
	def __init__(self):
		self.outputLogLevel = 1
		pass

	def setOutputLogLevel(self, loglevel):
		self.outputLogLevel = int(loglevel)

	def verbose(self, name, message):
		Log.Instance().printMessage(0, "VERBOSE", name, message, "grey")

	def info(self, name, message):
		Log.Instance().printMessage(1, "INFO", name, message, "normal")

	def warn(self, name, message):
		Log.Instance().printMessage(3, "WARNING", name, message, "yellow")

	def error(self, name, message):
		Log.Instance().printMessage(5, "ERROR", name, message, "red")

	def printMessage(self, level, logType, name, message, colorName):
		coloredMessage = bcolors(datetime.datetime.now().strftime("%H:%M:%S.%f") + " :: " + logType.ljust(9) +  " :: " + name + " :: " + message)

		if level >= self.outputLogLevel:
			print(coloredMessage.toColor(colorName))

		logger = logging.getLogger(name)
		logger.info(message)