import importlib
from Managers.Log import Log
from threading import Thread

class PluginManager:
	
	def __init__(self, cherryPyServer):
		self.loadedPlugins = []
		self.cherryPyServer = cherryPyServer
		
	def addPlugin(self, pluginName):
		pluginClass = self.importByName("Plugins." + pluginName, pluginName)
		plugin = pluginClass()
		plugin.mountToCherry(self.cherryPyServer)

		thread = Thread(target= plugin.start, args=())
		thread.start()

		self.loadedPlugins.append(plugin)

	def importByName(self, fullName, className):
		mod = __import__(fullName)
		components = fullName.split(".")

		for c in components[1:]:
			mod = getattr(mod, c)
		mod = getattr(mod, className)
		return mod

	def performEventBindings(self):
		for plugin in self.loadedPlugins:
			if plugin.eventBindings() is None:
				continue

			for identifier in plugin.eventBindings():
				Log.Instance().verbose(__name__, "Should perform bindings for " + plugin.identify()["Identifier"] + " to " + identifier)
				results = self.findPluginsByIdentifier(identifier)
				if results is None:
					continue

				for foundPlugin in results:
					plugin.bind(foundPlugin)

	def findPluginsByIdentifier(self, identifier):
		results = sorted(s for s in self.loadedPlugins if s.identify()["Identifier"] == identifier)

		if len(results) == 0:
			return None

		return results



	def cleanUp(self):
		for n in self.loadedPlugins:
			n.cleanUp()