#from pyrrd.rrd import DataSource, RRA, RRD
#import rrdtool
#import RRDtool
from Classes.rrdpy.rrdpy import rrdpy
import time
import os
from Managers.ConfigManager import ConfigManager
from Managers.Log import Log

class StorageManager:

	def __init__(self):
		self.rrdSettings = [{
								"datatype": ["temp"],
								"steps": 1,
								"sources": ["DS:value:GAUGE:600:U:U"],
								"value": ["RRA:LAST:0.0:1:3600", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
							},
							{
								"datatype": ["power.current.usage", "power.current.delivered"],
								"steps": 1,
								"sources": ["DS:value:GAUGE:600:U:U"],
								"value": ["RRA:LAST:0.0:1:3600", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
							},
							{
								"datatype": ["power.t1.usage", "power.t2.usage", "power.t1.delivered", "power.t2.delivered"],
								"steps": 1,
								"sources": ["DS:value:COUNTER:600:U:U"],
								"value": ["RRA:LAST:0.0:1:3600", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
							},
							{
								"datatype": ["cpuload", "memory.virtual", "memory.swap"],
								"steps": 1,
								"sources": ["DS:value:GAUGE:600:U:U"],
								"value": ["RRA:LAST:0.0:1:3600", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
							}]

	def storeRrdValue(self, nodeId, dataType, value):
		config = self.findRrdSetting(dataType)
		if config is None:
			Log.Instance().error(__name__, "Could not find rrd setting for this data type: " + dataType)
			return False

		# Log.Instance().verbose(__name__, "Found rrd setting for data type: " + dataType)
		rrdPath = ConfigManager().rrdPath(nodeId, dataType)
		intTime = int(time.time())

		rrd = rrdpy(rrdPath)
		if not os.path.exists(rrdPath):
			rrd.create(intTime, config["steps"], config["sources"], config["value"])

		rrd.update("value", None, value)

		return True

	#	def node_read(self, nodeId, dataType, resolution, startDate, endDate):
	def readRrdValue(self, nodeId, dataType, resolution, startDate, endDate):

		if endDate is None or endDate == 0 or endDate == "0":
			endDate = int(time.time())

		if startDate is None or startDate == 0 or startDate == "0":
			startDate = endDate - 3600

		rrdPath = ConfigManager().rrdPath(nodeId, dataType)
		if not os.path.exists(rrdPath):
			return None

		config = self.findRrdSetting(dataType)
		if config is None:
			Log.Instance().error(__name__, "Could not find rrd setting for this data type: " + dataType)
			return None

		rrd = rrdpy(rrdPath)
		results = rrd.fetch("AVERAGE", int(startDate), int(endDate), int(resolution))

		if (results is None):
			Log.Instance().error(__name__, "Error: " + rrd.lastError)
		return results

	def readLastRrdValue(self, nodeId, dataType, resolution):
		return []

	def findRrdSetting(self, dataType):
		result = sorted(s for s in self.rrdSettings for t in s["datatype"] if t == dataType)
		if len(result) == 0:
			return None
		return result[0]

