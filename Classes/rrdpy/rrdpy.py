from Classes.rrdpy.rra import rra
import rrdtool
import time
import re
import os

class rrdpy(object):

	def __init__(self, filename):
		self.filename = filename
		self.archives = []
		self.dataSources = []
		self.lastUpdated = 0
		self.lastError = None

		if os.path.exists(self.filename):
			self.populateInfo()

	def create(self, start, step, dataSources, archives):
		if start is None:
			startTime = int(time.time())
		else:
			startTime = start

		result = rrdtool.create(self.filename,
					   "--step", str(step),
					   "--start", str(startTime),
					   dataSources,
					   archives)

		if result:
			return None

		self.populateInfo()

	def populateInfo(self):
		information = rrdtool.info(self.filename)
		self.lastUpdated = information["last_update"]

		rraRegex = "^rra\[(.*?)\].*?$"
		dsRegex = "^ds\[(.*?)\].*?$"

		self.archives.clear()
		self.dataSources.clear()

		for k in information:
			match = re.search(rraRegex, k, re.I|re.M)
			if match:
				archiveIndex = int(match.groups()[0])
				archive = self.findArchiveByIndex(archiveIndex)
				if archive is None:
					archive = rra(archiveIndex)
					self.archives.append(archive)

				if k.endswith(".pdp_per_row"): archive.pdpPerRow = int(information[k])
				if k.endswith(".cf"): archive.CF = information[k]
				if k.endswith(".cur_row"): archive.currentRow = int(information[k])
				if k.endswith(".rows"): archive.rows = int(information[k])

			match = re.search(dsRegex, k, re.I|re.M)
			if match:
				if not match.groups()[0] in self.dataSources:
					self.dataSources.append(match.groups()[0])

	def update(self, dataSource, recordTime, value):
		if recordTime is None:
			recordTime = int(time.time())

		rrdtool.update(self.filename,
					   "-t", dataSource,
					   str(recordTime) + ":" + str(value))

	def fetch(self, CF, startTime, endTime, resolution):

		if startTime > endTime:
			self.lastError = "startTime cannot be greater than endTime"
			return None
#
#		self.populateInfo()
#		periodLength = endTime - startTime
#		archive = self.findArchiveByResolution(resolution, CF)

#		if archive is None:
#			self.lastError = "archive with this resolution (" + str(resolution) + ") not found"
#			return None

		results = rrdtool.fetch(self.filename,
					  CF,
					  "-r", str(resolution),
					  "-s", str(startTime),
					  "-e", str(endTime))

		if not results or len(results) == 0:
			return []

		index = 0
		returnValue = []

		returnedResolution = results[0][2]
		timestamp = results[0][0]

		while timestamp <= results[0][1]:
			if index >= len(results[2]):
				value = None
			else:
				value = results[2][index][0]

			returnValue.append({
				"timestamp" : timestamp,
				"value" : value
			})

			timestamp += returnedResolution
			index += 1

		return returnValue

	def findArchiveByResolution(self, resolution, CF):
		result = sorted(s for s in self.archives if s.pdpPerRow == resolution and s.CF == CF)
		if len(result) == 0:
			return None

		return result[0]

	def findArchiveByIndex(self, index):
		result = sorted(s for s in self.archives if s.index == index)
		if len(result) == 0:
			return None
		return result[0]