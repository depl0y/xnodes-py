from Classes.RegexCommand import RegexCommand
import re

class ArduinoString:
	def parseString(inputString):
		try:
			inputString = inputString.decode("utf-8").rstrip('\r\n ')
		except UnicodeDecodeError:
			return 0
		
		valueTypes = [
			RegexCommand("temp", "^TEMP:(?P<value>[0-9.]*?)$", ["value"]),
			RegexCommand("power", "^POWER:(?P<value>[0-9.]*?)$", ["value"]),
			RegexCommand("gas", "^GAS:(?P<value>[0-9.]*?)$", ["value"]),
			RegexCommand("power.t1.usage", "^1-0:1.8.1\((?P<value>[0-9.]*?)\*kWh\)$", ["value"]),
			RegexCommand("power.t2.usage", "^1-0:1.8.2\((?P<value>[0-9.]*?)\*kWh\)$", ["value"]),
			RegexCommand("power.current.usage", "^1-0:1.7.0\\((?P<value>[0-9.]*?)\\*kW\\)$", ["value"])
		]
		
		for r in valueTypes:
			match = re.match(r.regEx, inputString, re.M|re.I)

			if match:
				returnObject = {"type": r.dataType}
				
				for groupName in r.groupNames:
					returnObject[groupName] = match.group(groupName)
					
				return returnObject
				
		return 0