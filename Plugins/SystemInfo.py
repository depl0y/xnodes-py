import serial
import configparser
import time
import random
import http.client
import os
import psutil
from Managers.Log import Log
from Managers.ConfigManager import ConfigManager
from Classes.EventHook import EventHook


from Plugins import PluginBase

class SystemInfo(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 255
		self.onCpuInfo = EventHook()
		self.stop = False

	def defaultSettings(self):
		pass

	def identify(self):
		return {
			'Identifier': 'org.xnodes.systeminfo',
			'Version': '1.0'
		}

	def storagePath(self):
		pass

	def cleanUp(self):
		Log.Instance().info(__name__, "Cleaning up SystemInfoNode")
		self.stop = True
		pass

	def to_meg(self, n):
		return str(int(n / 1024 / 1024)) + "M"

	def pprint_ntuple(self, nt):
		for name in nt._fields:
			value = getattr(nt, name)
			if name != 'percent':
				value = self.to_meg(value)

	def getValue(self, result, name):
		#for fieldName in result._fields:
		return getattr(result, name)

	def start(self):
		self.startMonitoring()

	def startMonitoring(self):
		self.stop = False

		# Total, Available, Percent, Used, Free, Active, Inactive, Wired
		while not self.stop:
			cpuresult = psutil.cpu_percent(1, False)

			cpuObject = {
				"type": "cpuload",
				"value": cpuresult
			}

			self.sendObjectToApi(cpuObject)
			self.onCpuInfo.fire(cpuObject)

			result = psutil.virtual_memory()
			self.sendObjectToApi({
				"type": "memory.virtual",
				"value": self.getValue(result, "percent")
			})
			time.sleep(1)

		Log.Instance().warn(__name__, "System monitoring stopped")

	def sendObjectToApi(self, valueObject):
		getPath = "/api/node:store/" + str(self.pluginId) + "/" + valueObject["type"] + "/" + str(valueObject["value"])
		httpConnection = http.client.HTTPConnection(ConfigManager().baseUrl())
		httpConnection.request("GET", getPath)
		r1 = httpConnection.getresponse()
		if not r1.status == 200:
			Log.Instance().error(__name__, "Call to webserver failed with error " + str(r1.status) + " : " + r1.reason)
			Log.Instance().error(__name__, "\t " + ConfigManager().baseUrl() + getPath)
		else:
			Log.Instance().verbose(__name__, "Request completed: " + getPath)













