import serial
import configparser
import time
import random
import http.client
import os

from Managers.Log import Log
from Classes.ArduinoString  import ArduinoString
from Managers.ConfigManager import ConfigManager

from Plugins import PluginBase

class Random(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 255

	def defaultSettings(self):
		pass

	def identify(self):
		return {
			'Identifier': 'org.xnodes.random',
			'Version': '1.0'
		}

	def storagePath(self):
		pass

	def cleanUp(self):
		Log.Instance().info(__name__, "Cleaning up Random")
		pass

	def start(self):
		self.startMonitoring()

	def startMonitoring(self):
		stop = False

		while not stop:
			r = random.randint(0, 30)
			self.sendObjectToApi({
				"type": "temp",
				"value": r
			})
			time.sleep(1)

		Log.Instance().warn(__name__, "Monitoring of serial port was stopped.")

	def sendObjectToApi(self, valueObject):
		getPath = "/api/node:store/" + str(self.pluginId) + "/" + valueObject["type"] + "/" + str(valueObject["value"])
		httpConnection = http.client.HTTPConnection(ConfigManager().baseUrl())
		httpConnection.request("GET", getPath)
		r1 = httpConnection.getresponse()
		if not r1.status == 200:
			Log.error(__name__, "Call to webserver failed with error " + str(r1.status) + " : " + r1.reason)
			Log.error(__name__, "\t " + ConfigManager().baseUrl() + getPath)
		else:
			Log.verbose(__name__, "Request completed: " + getPath)













