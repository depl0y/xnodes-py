import serial
import configparser
import http.client
import os
import time

from Classes.EventHook import EventHook
from Managers.Log import Log
from Classes.ArduinoString  import ArduinoString
from Managers.ConfigManager import ConfigManager

from Plugins import PluginBase

class SerialArduino(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 0
		self.onDataReceived = EventHook()

	def defaultSettings(self):
		pass
		
	def identify(self):
		return {
			'Identifier': 'org.xnodes.serialarduino',
			'Version': '1.0'
		}
	
	def storagePath(self):
		pass
	
	def cleanUp(self):
		Log.Instance().info(__name__, "Cleaning up SerialArduino")

		if self.ser.isOpen():
			Log.Instance().info(__name__, "Closing serial port")
			self.ser.close();

		pass

	def start(self):
		self.startMonitoring()

	def startMonitoring(self):
		# Read port from config file
		config = configparser.ConfigParser()
		config.read("config")
		self.ser = serial.Serial()
		self.ser.port = config["SerialArduino"]["Port"]
		self.ser.baudrate = int(config["SerialArduino"]["BaudRate"])
		
		try:
			self.ser.open()
		except serial.serialutil.SerialException:
			Log.Instance().error(__name__, "Could not open serial port, exception occurred, retrying in 30 seconds")
			time.sleep(30)
			self.startMonitoring()
			return

		if not self.ser.isOpen():
			Log.Instance().error(__name__, "Could not open serial port, retrying in 30 seconds")
			time.sleep(30)
			self.startMonitoring()
			return
			
		Log.Instance().info(__name__, "Opened serial port " + self.ser.port + " @ " + str(self.ser.baudrate) + " bps")
		
		stop = False
		while not stop:
			try:
				readLine = self.ser.readline()
				parsedObject = ArduinoString.parseString(readLine)
				if not parsedObject == 0 and not parsedObject["type"] == "rf":
					parsedObject["value"] = float(parsedObject["value"])

					if parsedObject["type"].startswith("power."):
						parsedObject["value"] *= 1000

					if parsedObject["type"].startswith("power.t"):
						parsedObject["value"] = int(parsedObject["value"])

					self.onDataReceived.fire(parsedObject)
					self.sendObjectToApi(parsedObject)

			except BlockingIOError:
				Log.Instance().error(__name__, "Reading from serial port was blocked, port in use?")
				stop = True

		Log.Instance().warn(__name__, "Monitoring of serial port was stopped.")
				
	def sendObjectToApi(self, valueObject):
		getPath = "/api/node:store/" + str(self.pluginId) + "/" + valueObject["type"] + "/" + str(valueObject["value"])
		httpConnection = http.client.HTTPConnection(ConfigManager().baseUrl())
		httpConnection.request("GET", getPath)
		r1 = httpConnection.getresponse()
		if not r1.status == 200:
			Log.Instance().error(__name__, "Call to webserver failed with error " + str(r1.status) + " : " + r1.reason)
			Log.Instance().error(__name__, "\t " + ConfigManager().baseUrl() + getPath)
		else:
			Log.Instance().verbose(__name__, "Request completed: " + getPath)