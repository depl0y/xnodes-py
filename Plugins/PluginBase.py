import os

class PluginBase:
	def __init__(self):
		pass

	def start(self):
		pass

	def mountToCherry(self, cherry):
		pass
		
	def defaultSettings(self):
		pass
		
	def identify(self):
		raise Exception("Identifier method not overridden")
	
	def storagePath(self):
		pass
	
	def cleanUp(self):
		pass

	def eventBindings(self):
		return None

	def bind(self, plugin):
		pass