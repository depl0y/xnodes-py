import time
import random
import http.client
import os

from Managers.Log import Log
from Managers.ConfigManager import ConfigManager
from Plugins import PluginBase

class Thermostat(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 254
		self.monitorNodeId = 0

	def defaultSettings(self):
		pass

	def identify(self):
		return {
			'Identifier': 'org.xnodes.thermostat',
			'Version': '1.0'
		}

	def storagePath(self):
		pass

	def cleanUp(self):
		Log.Instance().info(__name__, "Cleaning up Thermostat")
		pass

	def start(self):
		self.startMonitoring()

	def startMonitoring(self):
		stop = False

	def onTemperatureReceived(self):
		pass

	def eventBindings(self):
		return ["org.xnodes.serialarduino", "org.xnodes.systeminfo"]

	def onDataReceivedFromArduino(self, dataObject):
		pass

	def onCpuInfoFromXnode(self, dataObject):
		pass

	def bind(self, plugin):
		pluginIdentifier = plugin.identify()["Identifier"]

		Log.Instance().info(__name__, "Binding to " + pluginIdentifier)

		if pluginIdentifier == "org.xnodes.serialarduino":
			plugin.onDataReceived += self.onDataReceivedFromArduino

		if pluginIdentifier == "org.xnodes.systeminfo":
			plugin.onCpuInfo += self.onCpuInfoFromXnode