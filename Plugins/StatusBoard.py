__author__ = 'depl0y'

import cherrypy
from Managers.Log import Log
import time
from Managers.StorageManager import StorageManager
import json

from Plugins import PluginBase

class StatusBoard(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 249
		Log.Instance().info(__name__, "Initializing StatusBoard plugin")

	def mountToCherry(self, cherry):
		cherry.tree.mount(self, "/statusboard")

	def cleanUp(self):
		pass

	def identify(self):
		return {
			"Identifier": "org.xnodes.statusboard",
		    "Version" : "1.0"
		}

	@cherrypy.expose
	def node_24h(self, nodeId, dataType, **kwargs):
		endDate = time.localtime()
		startDate = endDate - (24 * 60 * 60)

		result = StorageManager().readRrdValue(nodeId, dataType, 900, startDate, endDate)

		if result is None:
			return json.dumps({ "status" : "error" })
		else:
			return json.dumps(result)
