__author__ = 'depl0y'

import pybonjour
import configparser
import os

from Managers.Log import Log
from Managers.ConfigManager import ConfigManager
from Plugins import PluginBase

class Bonjour(PluginBase.PluginBase):
	def __init__(self):
		self.pluginId = 200
		self.serviceDescription = None

	def defaultSettings(self):
		pass

	def identify(self):
		return {
			'Identifier': 'org.xnodes.bonjour',
			'Version': '1.0'
		}

	def storagePath(self):
		pass

	def cleanUp(self):
		Log.Instance().info(__name__, "Cleaning up Bonjour")
		self.serviceDescription.close()
		pass

	def start(self):
		config = configparser.ConfigParser()
		config.read("config")

		self.serviceDescription = pybonjour.DNSServiceRegister(
 			callBack = register_callback,
			name = config['API']['BonjourName'],
			regtype = "_http._tcp",
			port = int(config['API']['Port']),
		)

def register_callback(sdRef, flags, errorCode, name, regtype, domain):
	if errorCode == pybonjour.kDNSServiceErr_NoError:
		print('Registered service:')
		print('  name    =', name)
		print('  regtype =', regtype)
		print('  domain  =', domain)
	else:
		Log.Instance().Info("Bonjour started")













