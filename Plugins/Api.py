import cherrypy
from Managers.StorageManager import StorageManager
from Managers.Log import Log
import simplejson
import json
from Plugins import PluginBase

import simplejson
def jsonp(func):
    def foo(self, *args, **kwargs):
        callback, _ = None, None
        if 'jsoncallback' in kwargs and '_' in kwargs:
            callback, _ = kwargs['jsoncallback'], kwargs['_']
            del kwargs['jsoncallback'], kwargs['_']
        ret = func(self, *args, **kwargs)
        if callback is not None:
            ret = '%s(%s)' % (callback, simplejson.dumps(ret))
        return ret
    return foo

class Api(PluginBase.PluginBase):

	def __init__(self):
		self.pluginId = 250
		Log.Instance().info(__name__, "Initializing API")
		self.lastValues = { "lastValues" : "" }
		
	def webRoutes(self):
		pass
		
	def defaultSettings(self):
		pass
		
	def identify(self):
		return {
			'Identifier': 'org.xnodes.api',
			'Version': '1.0'
		}
	
	def storagePath(self):
		pass

	def cleanUp(self):
		pass
		
	def mountToCherry(self, cherry):
		Log.Instance().info(__name__, "Mounting API to webserver")
		cherry.tree.mount(self, "/api")

	# /api/node:list
	@cherrypy.expose
	@jsonp
	def node_list(self, **kwargs):
		return json.dumps(dict(t="node list"))
	
	# /api/node:register(/1)
	@cherrypy.expose
	@jsonp
	def node_register(self, nodeId = 0, **kwargs):
		return json.dumps(dict(t = nodeId))
	
	# /api/node:store/:node/:type/:value
	@cherrypy.expose
	@jsonp
	def node_store(self, nodeId, dataType, value, **kwargs):
		storageKey = str(nodeId) + ":" + dataType
		self.lastValues[storageKey] = value

		if StorageManager().storeRrdValue(nodeId, dataType, value):
			Log.Instance().verbose(__name__, str(nodeId) + " :: Stored " + dataType + " value in RRD database")
			return json.dumps({ "status" : "ok" })
		else:
			Log.Instance().error(__name__, str(nodeId) + " :: Failure storing " + dataType + " value in RRD database")
			return json.dumps({ "status" : "error" })

	#  /api/node:types/:node
	@cherrypy.expose
	@jsonp
	def node_types(self, nodeId, **kwargs):
		return json.dumps(dict(id = nodeId))
	
	# /api/node:remove/:node
	@cherrypy.expose
	@jsonp
	def node_remove(self, nodeId, **kwargs):
		return json.dumps(dict(id = nodeId))
	
	# /api/node:read/:node/:type/:resolution/:startdate/:enddate
	@cherrypy.expose
	@jsonp
	def node_read(self, nodeId, dataType, resolution, startDate, endDate, **kwargs):
		Log.Instance().verbose(__name__, "Read request for node " + str(nodeId) + " : " + dataType + ":" + str(resolution) + ":" + str(startDate) + ":" + str(endDate))
		result = StorageManager().readRrdValue(nodeId, dataType, resolution, startDate, endDate)
		if result is None:
			return json.dumps({ "status" : "error" })
		else:
			return json.dumps(result)

	# /api/node:last/:node/:type/:resolution
	@cherrypy.expose
	@jsonp
	def node_last(self, nodeId, dataType, resolution, **kwargs):
		storageKey = str(nodeId) + ":" + dataType
		if storageKey in self.lastValues.keys():
			return json.dumps({ "value" : float(self.lastValues[storageKey]) })

		return json.dumps(dict({ "value" : 0 }))
	
	# /api/rf:list
	@cherrypy.expose
	@jsonp
	def rf_list(self, **kwargs):
		return json.dumps(dict(t = "RF list"))