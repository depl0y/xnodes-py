import cherrypy
import signal
import sys
import os
import configparser

from Classes.StaticContent import StaticContent
from Managers.DatabaseManager import DatabaseManager
from Managers.Log import Log
from Managers.PluginManager import PluginManager

def signal_handler(signal, frame):
	Log.Instance().info(__name__, "Shutting down xnodes")
	cherrypy.engine.stop()
	cherrypy.engine.exit()
	pluginManager.cleanUp()
	DatabaseManager.Instance().close()
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

os.system('clear')

config = configparser.ConfigParser()
config.read("config")

Log.Instance().setOutputLogLevel(int(config["Logging"]["Level"]))
Log.Instance().info(__name__, "Starting xnodes")

DatabaseManager.Instance()

current_dir = os.path.dirname(os.path.abspath(__file__))

conf = {
	"global": {
		"server.socket_host": config['API']['Host'],
		"server.socket_port": int(config['API']['Port'])
	}
}

staticdir = os.path.join(current_dir, "HTML")
Log.Instance().info(__name__, "Setting static dir to: " + staticdir)

staticconfig = {
	"/" : {
		"tools.staticdir.root" : current_dir,
		"tools.encode.on" : True
	},
	"/html": {
		"tools.staticdir.on": True,
		"tools.staticdir.dir": "HTML",
		"tools.staticdir.index"	: 'index.html'
	}
}

cherrypy.config.update(conf)
if config["Logging"]["CherryPy"] == "off":
	cherrypy.log.access_file = None
	cherrypy.log.screen = None

cherrypy.engine.start()

cherrypy.tree.mount(StaticContent(), "/", staticconfig)
Log.Instance().info(__name__, "Started webserver on " + conf["global"]["server.socket_host"] + ":" + str(
	conf["global"]["server.socket_port"]))

pluginManager = PluginManager(cherrypy)
pluginManager.addPlugin("Api")
pluginManager.addPlugin("Bonjour")
pluginManager.addPlugin("SerialArduino")
pluginManager.addPlugin("SystemInfo")
pluginManager.addPlugin("Thermostat")

if config["Other"]["EnableRandomNode"] == "yes":
	print("Should start RandomNode")
	pluginManager.addPlugin("Random")

pluginManager.performEventBindings()